﻿namespace test_SE
{
    partial class ListOfRequests
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DataGridPanel = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Firstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lastname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Transportation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listOfRequestsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listOfReqViewDataset = new test_SE.listOfReqViewDataset();
            this.ButtonPanel = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.closeRequestbtn = new System.Windows.Forms.Button();
            this.tempRemovebtn = new System.Windows.Forms.Button();
            this.editbtn = new System.Windows.Forms.Button();
            this.newRequestbtn = new System.Windows.Forms.Button();
            this.listOfRequestsTableAdapter = new test_SE.listOfReqViewDatasetTableAdapters.listOfRequestsTableAdapter();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ssnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reqDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalcostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waysDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listOfRequestsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listOfReqViewDataset)).BeginInit();
            this.ButtonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridPanel
            // 
            this.DataGridPanel.Controls.Add(this.dataGridView1);
            this.DataGridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridPanel.Location = new System.Drawing.Point(0, 0);
            this.DataGridPanel.Name = "DataGridPanel";
            this.DataGridPanel.Size = new System.Drawing.Size(947, 478);
            this.DataGridPanel.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Status,
            this.Firstname,
            this.Lastname,
            this.SSN,
            this.Date,
            this.TotalValue,
            this.From,
            this.Transportation,
            this.idDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.fnameDataGridViewTextBoxColumn,
            this.lnameDataGridViewTextBoxColumn,
            this.ssnDataGridViewTextBoxColumn,
            this.reqDateDataGridViewTextBoxColumn,
            this.totalcostDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.waysDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.listOfRequestsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(947, 478);
            this.dataGridView1.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "Id";
            this.ID.HeaderText = "شماره";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.HeaderText = "وضعیت";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // Firstname
            // 
            this.Firstname.DataPropertyName = "fname";
            this.Firstname.HeaderText = "نام";
            this.Firstname.Name = "Firstname";
            this.Firstname.ReadOnly = true;
            // 
            // Lastname
            // 
            this.Lastname.DataPropertyName = "lname";
            this.Lastname.HeaderText = "نام خانوادگی";
            this.Lastname.Name = "Lastname";
            this.Lastname.ReadOnly = true;
            // 
            // SSN
            // 
            this.SSN.DataPropertyName = "ssn";
            this.SSN.HeaderText = "شماره ملی";
            this.SSN.Name = "SSN";
            this.SSN.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "reqDate";
            this.Date.HeaderText = "تاریخ";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // TotalValue
            // 
            this.TotalValue.DataPropertyName = "totalcost";
            this.TotalValue.HeaderText = "ارزش کل";
            this.TotalValue.Name = "TotalValue";
            this.TotalValue.ReadOnly = true;
            // 
            // From
            // 
            this.From.DataPropertyName = "title";
            this.From.HeaderText = "از کشور";
            this.From.Name = "From";
            this.From.ReadOnly = true;
            // 
            // Transportation
            // 
            this.Transportation.DataPropertyName = "ways";
            this.Transportation.HeaderText = "از راه";
            this.Transportation.Name = "Transportation";
            this.Transportation.ReadOnly = true;
            // 
            // listOfRequestsBindingSource
            // 
            this.listOfRequestsBindingSource.DataMember = "listOfRequests";
            this.listOfRequestsBindingSource.DataSource = this.listOfReqViewDataset;
            // 
            // listOfReqViewDataset
            // 
            this.listOfReqViewDataset.DataSetName = "listOfReqViewDataset";
            this.listOfReqViewDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ButtonPanel
            // 
            this.ButtonPanel.Controls.Add(this.button3);
            this.ButtonPanel.Controls.Add(this.closeRequestbtn);
            this.ButtonPanel.Controls.Add(this.tempRemovebtn);
            this.ButtonPanel.Controls.Add(this.editbtn);
            this.ButtonPanel.Controls.Add(this.newRequestbtn);
            this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonPanel.Location = new System.Drawing.Point(0, 478);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.Size = new System.Drawing.Size(947, 47);
            this.ButtonPanel.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(22, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "خروج";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // closeRequestbtn
            // 
            this.closeRequestbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeRequestbtn.Location = new System.Drawing.Point(533, 12);
            this.closeRequestbtn.Name = "closeRequestbtn";
            this.closeRequestbtn.Size = new System.Drawing.Size(96, 23);
            this.closeRequestbtn.TabIndex = 0;
            this.closeRequestbtn.Text = "بستن اظهارنامه";
            this.closeRequestbtn.UseVisualStyleBackColor = true;
            this.closeRequestbtn.Click += new System.EventHandler(this.closeRequestbtn_Click);
            // 
            // tempRemovebtn
            // 
            this.tempRemovebtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tempRemovebtn.Location = new System.Drawing.Point(635, 12);
            this.tempRemovebtn.Name = "tempRemovebtn";
            this.tempRemovebtn.Size = new System.Drawing.Size(96, 23);
            this.tempRemovebtn.TabIndex = 0;
            this.tempRemovebtn.Text = "ابطال";
            this.tempRemovebtn.UseVisualStyleBackColor = true;
            // 
            // editbtn
            // 
            this.editbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.editbtn.Location = new System.Drawing.Point(737, 12);
            this.editbtn.Name = "editbtn";
            this.editbtn.Size = new System.Drawing.Size(96, 23);
            this.editbtn.TabIndex = 0;
            this.editbtn.Text = "ویرایش";
            this.editbtn.UseVisualStyleBackColor = true;
            this.editbtn.Click += new System.EventHandler(this.editbtn_Click);
            // 
            // newRequestbtn
            // 
            this.newRequestbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.newRequestbtn.Location = new System.Drawing.Point(839, 12);
            this.newRequestbtn.Name = "newRequestbtn";
            this.newRequestbtn.Size = new System.Drawing.Size(96, 23);
            this.newRequestbtn.TabIndex = 0;
            this.newRequestbtn.Text = "اظهارنامه جدید";
            this.newRequestbtn.UseVisualStyleBackColor = true;
            this.newRequestbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // listOfRequestsTableAdapter
            // 
            this.listOfRequestsTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fnameDataGridViewTextBoxColumn
            // 
            this.fnameDataGridViewTextBoxColumn.DataPropertyName = "fname";
            this.fnameDataGridViewTextBoxColumn.HeaderText = "fname";
            this.fnameDataGridViewTextBoxColumn.Name = "fnameDataGridViewTextBoxColumn";
            this.fnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lnameDataGridViewTextBoxColumn
            // 
            this.lnameDataGridViewTextBoxColumn.DataPropertyName = "lname";
            this.lnameDataGridViewTextBoxColumn.HeaderText = "lname";
            this.lnameDataGridViewTextBoxColumn.Name = "lnameDataGridViewTextBoxColumn";
            this.lnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ssnDataGridViewTextBoxColumn
            // 
            this.ssnDataGridViewTextBoxColumn.DataPropertyName = "ssn";
            this.ssnDataGridViewTextBoxColumn.HeaderText = "ssn";
            this.ssnDataGridViewTextBoxColumn.Name = "ssnDataGridViewTextBoxColumn";
            this.ssnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reqDateDataGridViewTextBoxColumn
            // 
            this.reqDateDataGridViewTextBoxColumn.DataPropertyName = "reqDate";
            this.reqDateDataGridViewTextBoxColumn.HeaderText = "reqDate";
            this.reqDateDataGridViewTextBoxColumn.Name = "reqDateDataGridViewTextBoxColumn";
            this.reqDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalcostDataGridViewTextBoxColumn
            // 
            this.totalcostDataGridViewTextBoxColumn.DataPropertyName = "totalcost";
            this.totalcostDataGridViewTextBoxColumn.HeaderText = "totalcost";
            this.totalcostDataGridViewTextBoxColumn.Name = "totalcostDataGridViewTextBoxColumn";
            this.totalcostDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // waysDataGridViewTextBoxColumn
            // 
            this.waysDataGridViewTextBoxColumn.DataPropertyName = "ways";
            this.waysDataGridViewTextBoxColumn.HeaderText = "ways";
            this.waysDataGridViewTextBoxColumn.Name = "waysDataGridViewTextBoxColumn";
            this.waysDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ListOfRequests
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 525);
            this.Controls.Add(this.DataGridPanel);
            this.Controls.Add(this.ButtonPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ListOfRequests";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "لیست اظهارنامه‌ها";
            this.Load += new System.EventHandler(this.ListOfRequests_Load);
            this.DataGridPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listOfRequestsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listOfReqViewDataset)).EndInit();
            this.ButtonPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel DataGridPanel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel ButtonPanel;
        private System.Windows.Forms.Button newRequestbtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button closeRequestbtn;
        private System.Windows.Forms.Button tempRemovebtn;
        private System.Windows.Forms.Button editbtn;
        private listOfReqViewDataset listOfReqViewDataset;
        private System.Windows.Forms.BindingSource listOfRequestsBindingSource;
        private listOfReqViewDatasetTableAdapters.listOfRequestsTableAdapter listOfRequestsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Firstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lastname;
        private System.Windows.Forms.DataGridViewTextBoxColumn SSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn Transportation;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ssnDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reqDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalcostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn waysDataGridViewTextBoxColumn;
    }
}