﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_SE.Model;
using System.Data;

namespace test_SE.Controller
{
    class AuthController
    {
        private List<auth> users = new List<auth>();
        private auth loggedInUser { get; set; }
        private static AuthController instance = null;
        
        private AuthController()
        {
            this.loggedInUser = null;
        }

        public static AuthController getInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AuthController();
                }
                return instance;
            }
        }

        private void fillData()
        {
        }

        public Boolean login(string username, string password){
            auth user = auth.getUser(username, password);
            if(user != null)
            {
                this.loggedInUser = user;
                return true;
            }
            return false;
        }

        public auth getLoggedInUser()
        {
            return this.loggedInUser;
        }

        public bool create(string username, string password, string type)
        {
            try
            {
                DBConnection db = DBConnection.getInstance();
                db.cmm.CommandText = "INSERT INTO AUTH VALUES ('" + username + "' , '" + password + "' ,'" + type + "')";
                db.cmm.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }

            
        }
    }
}
