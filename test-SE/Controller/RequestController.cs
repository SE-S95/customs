﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_SE.Model;
namespace test_SE.Controller
{
    public class RequestController
    {
        private List<Country> countries = null;
        private List<Good> goods = null;

        private static RequestController instance = null;
        private RequestController()
        {
            this.fillData();
        }
        
        public static RequestController getInstance()
        {
                if (instance == null)
                {
                    instance = new RequestController();
                }
                return instance;
        }

        private void fillData()
        {
            getCountries();
            getGoods();
        }

        List<Country> getCountries()
        {
            if(this.countries == null)
            {
                countries = Country.all();
            }
            return countries;
        }

        List<Good> getGoods()
        {
            if(this.goods == null)
            {
                goods = Good.all();
            }
            return goods;
        }
        Good newGood(int parent, string name)
        {
            return null;
        }

        List<Businessman> getBussMan()
        {
            return null;
        }

        public int getLastRuleId()
        {
            DBConnection db = DBConnection.getInstance();
            db.cmm.CommandText = "SELECT MAX(ID) FROM [RULE]";
            return (int) db.cmm.ExecuteScalar();
        }

        public string getBusinessmanName(int id)
        {
            DBConnection db = DBConnection.getInstance();
            db.cmm.CommandText = "select (fname + ' ' + lname) name from bussinesman where id = " + id.ToString();
            return (string)db.cmm.ExecuteScalar();
        }

        public int getLastReqId()
        {
            DBConnection db = DBConnection.getInstance();
            db.cmm.CommandText = "SELECT MAX(ID) FROM [request]";
            return (int)db.cmm.ExecuteScalar();
        }

        public void insertRules(int req_id)
        {
            DBConnection db = DBConnection.getInstance();
            db.cmm.CommandText = "insert into required_per " +
                                  "select distinct re.id ,p.id from[rule] ru, request re, Permission p, rule_item rui where rui.permission_id = p.id and rui.rule_id = ru.id and " +
                                  "re.id = " + req_id.ToString() + " and " +
                                  "(ISNULL(ru.country, re.country) = re.country and " +
                                  "ISNULL(ru.ways, re.ways) = re.ways and " +
                                  "ISNULL(ru.weight, 0) < (select sum(weight) from request_item where request_item.request_id = re.Id) and " +
                                  "ISNULL(ru.quantity, 0) < (select sum(weight) from request_item where request_item.request_id = re.Id)) and " +
                                  "ISNULL(ru.price, 0) < re.totalcost and " +
                                  "re.reqDate BETWEEN ISNULL(ru.[from], re.reqDate) and ISNULL(ru.[to], DATEADD(d, 1, re.reqDate) ) and " +
                                  "  (ru.good is null or ru.good in (select g.parentID from good g, request_item ri where g.id = ri.good_id and ri.request_id = re.Id))";
            db.cmm.ExecuteNonQuery();
        }

        public bool closeRequest(int req_id)
        {

            return true;
        }
    }
}
