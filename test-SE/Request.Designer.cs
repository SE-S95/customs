﻿namespace test_SE
{
    partial class Request
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bussinesmanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sadDataSet3 = new test_SE.sadDataSet3();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.countryCombo = new System.Windows.Forms.ComboBox();
            this.countryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sadDataSet2 = new test_SE.sadDataSet2();
            this.first_l = new System.Windows.Forms.Label();
            this.transportationType = new System.Windows.Forms.GroupBox();
            this.groundRadio = new System.Windows.Forms.RadioButton();
            this.airRadio = new System.Windows.Forms.RadioButton();
            this.seaRadio = new System.Windows.Forms.RadioButton();
            this.country_l = new System.Windows.Forms.Label();
            this.first_t = new System.Windows.Forms.TextBox();
            this.data_l = new System.Windows.Forms.Label();
            this.ssn_l = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.goodidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.goodBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.sadDataSet = new test_SE.sadDataSet();
            this.weightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestitemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.requestDataSet = new test_SE.requestDataSet();
            this.goodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ButtonPanel = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.GridPanel = new System.Windows.Forms.Panel();
            this.bussinesmanTableAdapter = new test_SE.sadDataSet3TableAdapters.bussinesmanTableAdapter();
            this.goodTableAdapter = new test_SE.sadDataSetTableAdapters.GoodTableAdapter();
            this.request_itemTableAdapter = new test_SE.requestDataSetTableAdapters.request_itemTableAdapter();
            this.requestTableAdapter1 = new test_SE.requestDataSetTableAdapters.requestTableAdapter();
            this.countryTableAdapter = new test_SE.sadDataSet2TableAdapters.countryTableAdapter();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bussinesmanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet2)).BeginInit();
            this.transportationType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestitemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodBindingSource)).BeginInit();
            this.ButtonPanel.SuspendLayout();
            this.GridPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.countryCombo);
            this.groupBox1.Controls.Add(this.first_l);
            this.groupBox1.Controls.Add(this.transportationType);
            this.groupBox1.Controls.Add(this.country_l);
            this.groupBox1.Controls.Add(this.first_t);
            this.groupBox1.Controls.Add(this.data_l);
            this.groupBox1.Controls.Add(this.ssn_l);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(805, 190);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مشخصات تاجر";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.bussinesmanBindingSource;
            this.comboBox1.DisplayMember = "ssn";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(521, 30);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(173, 21);
            this.comboBox1.TabIndex = 53;
            this.comboBox1.ValueMember = "Id";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // bussinesmanBindingSource
            // 
            this.bussinesmanBindingSource.DataMember = "bussinesman";
            this.bussinesmanBindingSource.DataSource = this.sadDataSet3;
            // 
            // sadDataSet3
            // 
            this.sadDataSet3.DataSetName = "sadDataSet3";
            this.sadDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "ارزش کل کالاها";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(6, 163);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 21);
            this.textBox2.TabIndex = 52;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker1.Location = new System.Drawing.Point(275, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(132, 21);
            this.dateTimePicker1.TabIndex = 48;
            // 
            // countryCombo
            // 
            this.countryCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.countryCombo.DataSource = this.countryBindingSource;
            this.countryCombo.DisplayMember = "title";
            this.countryCombo.FormattingEnabled = true;
            this.countryCombo.Location = new System.Drawing.Point(275, 59);
            this.countryCombo.Name = "countryCombo";
            this.countryCombo.Size = new System.Drawing.Size(132, 21);
            this.countryCombo.TabIndex = 46;
            this.countryCombo.ValueMember = "Id";
            // 
            // countryBindingSource
            // 
            this.countryBindingSource.DataMember = "country";
            this.countryBindingSource.DataSource = this.sadDataSet2;
            // 
            // sadDataSet2
            // 
            this.sadDataSet2.DataSetName = "sadDataSet2";
            this.sadDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // first_l
            // 
            this.first_l.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.first_l.AutoSize = true;
            this.first_l.Location = new System.Drawing.Point(705, 86);
            this.first_l.Name = "first_l";
            this.first_l.Size = new System.Drawing.Size(89, 13);
            this.first_l.TabIndex = 37;
            this.first_l.Text = "نام و نام خانوادگی";
            // 
            // transportationType
            // 
            this.transportationType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transportationType.Controls.Add(this.groundRadio);
            this.transportationType.Controls.Add(this.airRadio);
            this.transportationType.Controls.Add(this.seaRadio);
            this.transportationType.Location = new System.Drawing.Point(195, 101);
            this.transportationType.Name = "transportationType";
            this.transportationType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.transportationType.Size = new System.Drawing.Size(281, 45);
            this.transportationType.TabIndex = 45;
            this.transportationType.TabStop = false;
            this.transportationType.Text = "نحوه ورود به کشور";
            // 
            // groundRadio
            // 
            this.groundRadio.AutoSize = true;
            this.groundRadio.Location = new System.Drawing.Point(209, 18);
            this.groundRadio.Name = "groundRadio";
            this.groundRadio.Size = new System.Drawing.Size(54, 17);
            this.groundRadio.TabIndex = 2;
            this.groundRadio.TabStop = true;
            this.groundRadio.Text = "زمینی";
            this.groundRadio.UseVisualStyleBackColor = true;
            // 
            // airRadio
            // 
            this.airRadio.AutoSize = true;
            this.airRadio.Location = new System.Drawing.Point(99, 20);
            this.airRadio.Name = "airRadio";
            this.airRadio.Size = new System.Drawing.Size(55, 17);
            this.airRadio.TabIndex = 1;
            this.airRadio.TabStop = true;
            this.airRadio.Text = "هوایی";
            this.airRadio.UseVisualStyleBackColor = true;
            // 
            // seaRadio
            // 
            this.seaRadio.AutoSize = true;
            this.seaRadio.Location = new System.Drawing.Point(7, 19);
            this.seaRadio.Name = "seaRadio";
            this.seaRadio.Size = new System.Drawing.Size(54, 17);
            this.seaRadio.TabIndex = 0;
            this.seaRadio.TabStop = true;
            this.seaRadio.Text = "دریایی";
            this.seaRadio.UseVisualStyleBackColor = true;
            // 
            // country_l
            // 
            this.country_l.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.country_l.AutoSize = true;
            this.country_l.Location = new System.Drawing.Point(421, 62);
            this.country_l.Name = "country_l";
            this.country_l.Size = new System.Drawing.Size(55, 13);
            this.country_l.TabIndex = 43;
            this.country_l.Text = "کشور مبدا";
            // 
            // first_t
            // 
            this.first_t.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.first_t.BackColor = System.Drawing.SystemColors.Info;
            this.first_t.Location = new System.Drawing.Point(521, 83);
            this.first_t.Name = "first_t";
            this.first_t.ReadOnly = true;
            this.first_t.Size = new System.Drawing.Size(173, 21);
            this.first_t.TabIndex = 38;
            // 
            // data_l
            // 
            this.data_l.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.data_l.AutoSize = true;
            this.data_l.Location = new System.Drawing.Point(422, 22);
            this.data_l.Name = "data_l";
            this.data_l.Size = new System.Drawing.Size(54, 13);
            this.data_l.TabIndex = 41;
            this.data_l.Text = "تاریخ اظهار";
            // 
            // ssn_l
            // 
            this.ssn_l.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ssn_l.AutoSize = true;
            this.ssn_l.Location = new System.Drawing.Point(728, 30);
            this.ssn_l.Name = "ssn_l";
            this.ssn_l.Size = new System.Drawing.Size(66, 13);
            this.ssn_l.TabIndex = 39;
            this.ssn_l.Text = "شناسه ملی";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.goodidDataGridViewTextBoxColumn,
            this.weightDataGridViewTextBoxColumn,
            this.companyDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.requestitemBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(805, 244);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            // 
            // goodidDataGridViewTextBoxColumn
            // 
            this.goodidDataGridViewTextBoxColumn.DataPropertyName = "good_id";
            this.goodidDataGridViewTextBoxColumn.DataSource = this.goodBindingSource1;
            this.goodidDataGridViewTextBoxColumn.DisplayMember = "title";
            this.goodidDataGridViewTextBoxColumn.HeaderText = "کالا";
            this.goodidDataGridViewTextBoxColumn.Name = "goodidDataGridViewTextBoxColumn";
            this.goodidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.goodidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.goodidDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // goodBindingSource1
            // 
            this.goodBindingSource1.DataMember = "Good";
            this.goodBindingSource1.DataSource = this.sadDataSet;
            // 
            // sadDataSet
            // 
            this.sadDataSet.DataSetName = "sadDataSet";
            this.sadDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // weightDataGridViewTextBoxColumn
            // 
            this.weightDataGridViewTextBoxColumn.DataPropertyName = "weight";
            this.weightDataGridViewTextBoxColumn.HeaderText = "وزن";
            this.weightDataGridViewTextBoxColumn.Name = "weightDataGridViewTextBoxColumn";
            // 
            // companyDataGridViewTextBoxColumn
            // 
            this.companyDataGridViewTextBoxColumn.DataPropertyName = "company";
            this.companyDataGridViewTextBoxColumn.HeaderText = "نام شرکت";
            this.companyDataGridViewTextBoxColumn.Name = "companyDataGridViewTextBoxColumn";
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "تعداد";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "قیمت واحد";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            // 
            // requestitemBindingSource
            // 
            this.requestitemBindingSource.DataMember = "request_item";
            this.requestitemBindingSource.DataSource = this.requestDataSet;
            // 
            // requestDataSet
            // 
            this.requestDataSet.DataSetName = "requestDataSet";
            this.requestDataSet.EnforceConstraints = false;
            this.requestDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // goodBindingSource
            // 
            this.goodBindingSource.DataMember = "Good";
            this.goodBindingSource.DataSource = this.sadDataSet;
            // 
            // ButtonPanel
            // 
            this.ButtonPanel.Controls.Add(this.button3);
            this.ButtonPanel.Controls.Add(this.button2);
            this.ButtonPanel.Controls.Add(this.button1);
            this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonPanel.Location = new System.Drawing.Point(0, 440);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.Size = new System.Drawing.Size(805, 41);
            this.ButtonPanel.TabIndex = 53;
            this.ButtonPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonPanel_Paint);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(92, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "بررسی مجوز‌ها";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(643, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "انصراف";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(724, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "ذخیره";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GridPanel
            // 
            this.GridPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridPanel.Controls.Add(this.dataGridView1);
            this.GridPanel.Location = new System.Drawing.Point(0, 196);
            this.GridPanel.Name = "GridPanel";
            this.GridPanel.Size = new System.Drawing.Size(805, 244);
            this.GridPanel.TabIndex = 54;
            // 
            // bussinesmanTableAdapter
            // 
            this.bussinesmanTableAdapter.ClearBeforeFill = true;
            // 
            // goodTableAdapter
            // 
            this.goodTableAdapter.ClearBeforeFill = true;
            // 
            // request_itemTableAdapter
            // 
            this.request_itemTableAdapter.ClearBeforeFill = true;
            // 
            // requestTableAdapter1
            // 
            this.requestTableAdapter1.ClearBeforeFill = true;
            // 
            // countryTableAdapter
            // 
            this.countryTableAdapter.ClearBeforeFill = true;
            // 
            // Request
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 481);
            this.Controls.Add(this.GridPanel);
            this.Controls.Add(this.ButtonPanel);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(761, 460);
            this.Name = "Request";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "فرم اظهارنامه";
            this.Load += new System.EventHandler(this.Request_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bussinesmanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet2)).EndInit();
            this.transportationType.ResumeLayout(false);
            this.transportationType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestitemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodBindingSource)).EndInit();
            this.ButtonPanel.ResumeLayout(false);
            this.GridPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox countryCombo;
        private System.Windows.Forms.Label first_l;
        private System.Windows.Forms.GroupBox transportationType;
        private System.Windows.Forms.RadioButton groundRadio;
        private System.Windows.Forms.RadioButton airRadio;
        private System.Windows.Forms.RadioButton seaRadio;
        private System.Windows.Forms.Label country_l;
        private System.Windows.Forms.TextBox first_t;
        private System.Windows.Forms.Label data_l;
        private System.Windows.Forms.Label ssn_l;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel ButtonPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel GridPanel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox comboBox1;
        private sadDataSet3 sadDataSet3;
        private System.Windows.Forms.BindingSource bussinesmanBindingSource;
        private sadDataSet3TableAdapters.bussinesmanTableAdapter bussinesmanTableAdapter;
        private sadDataSet sadDataSet;
        private System.Windows.Forms.BindingSource goodBindingSource;
        private sadDataSetTableAdapters.GoodTableAdapter goodTableAdapter;
        private requestDataSet requestDataSet;
        private System.Windows.Forms.BindingSource requestitemBindingSource;
        private requestDataSetTableAdapters.request_itemTableAdapter request_itemTableAdapter;
        private System.Windows.Forms.BindingSource goodBindingSource1;
        private System.Windows.Forms.DataGridViewComboBoxColumn goodidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private requestDataSetTableAdapters.requestTableAdapter requestTableAdapter1;
        private sadDataSet2 sadDataSet2;
        private System.Windows.Forms.BindingSource countryBindingSource;
        private sadDataSet2TableAdapters.countryTableAdapter countryTableAdapter;
    }
}