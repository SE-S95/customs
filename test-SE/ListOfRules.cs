﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_SE
{
    public partial class ListOfRules : Form
    {
        public ListOfRules()
        {
            InitializeComponent();
        }

        private void exitbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newbtn_Click(object sender, EventArgs e)
        {
            NewRule f = new NewRule();
            f.ShowDialog();
        }

        private void ListOfRules_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'listOfRulesViewDataset.listOfRules' table. You can move, or remove it, as needed.
            this.listOfRulesTableAdapter.Fill(this.listOfRulesViewDataset.listOfRules);

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
