﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using test_SE.Controller;
using test_SE.Model;
namespace test_SE
{
    public partial class MainPage : Form
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void خروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void تعریفمجوزToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListOfPermissions lop = new ListOfPermissions();
            lop.can_add = true;
            lop.ShowDialog();
            this.Show();
        }

        private void لیستاظهارنامههاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListOfRequests r = new ListOfRequests();
            r.ShowDialog();
            this.Show();
        }

        private void جدیدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Request r = new Request();
            r.ShowDialog();
            this.Show();
        }

        private void MainPage_Load(object sender, EventArgs e)
        {
            AuthController authCtrl = AuthController.getInstance;
            userID.Text = authCtrl.getLoggedInUser().user_id.ToString();
            userName.Text = authCtrl.getLoggedInUser().username;
            switch (AuthController.getInstance.getLoggedInUser().GetType().Name)
            {
                case "GovUser":
                    اظهارنامهToolStripMenuItem.Visible = false;
                    مدیریتقوانینToolStripMenuItem.Visible = false;
                    تعریفمجوزToolStripMenuItem.Visible = false;
                    اضافهکردنکشورToolStripMenuItem.Visible = false;
                    اضافهکردنکالاToolStripMenuItem.Visible = false;
                    تعریفکاربرجدیدToolStripMenuItem.Visible = false;
                    break;

                case "LegislatorUser":
                    لیستتجادToolStripMenuItem.Visible = false;
                    break;

                case "CustomUser":
                    مدیریتسیستمToolStripMenuItem.Visible = false;
                    break;
            }
            
        }

        private void افزودنتاجرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListOfBussinesmans listofb = new ListOfBussinesmans();
            listofb.ShowDialog();
            this.Show();
        }

        private void لیستتجادToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListOfBussinesmans listofbs = new ListOfBussinesmans();
            listofbs.can_issue_permission = true;
            this.Hide();
            listofbs.ShowDialog();
            this.Show();
        }

        private void مدیریتقوانینToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListOfRules r = new ListOfRules();
            this.Hide();
            r.ShowDialog();
            this.Show();
        }

        private void اضافهکردنکشورToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListOfCountries c = new ListOfCountries();
            this.Hide();
            c.ShowDialog();
            this.Show();
        }

        private void لیستکالاهایواردشدهتوسطتاجرخاصToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void اضافهکردنکالاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListOfGoods g = new ListOfGoods();
            this.Hide();
            g.ShowDialog();
            this.Show();
        }

        private void تعریفکاربرجدیدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newUser nu = new newUser();
            this.Hide();
            nu.ShowDialog();
            this.Show();
        }
    }
}
