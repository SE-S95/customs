﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using test_SE.Controller;
namespace test_SE
{
    public partial class ListOfRequests : Form
    {
        public ListOfRequests()
        {
            InitializeComponent();
        }
        private void updateDataSet()
        {
            
            this.listOfRequestsTableAdapter.Fill(this.listOfReqViewDataset.listOfRequests);
            foreach(DataRow dr in listOfReqViewDataset.listOfRequests)
            {
                if ((int)dr["status"] == 1)
                    dataGridView1.Rows[listOfReqViewDataset.listOfRequests.Rows.IndexOf(dr)].Cells[1].Value = "باز";
            }
        }
        private void ListOfRequests_Load(object sender, EventArgs e)
        {
            updateDataSet();
        }

        private void ssn_l_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        public void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Request r = new Request();
            r.ShowDialog();
            this.Show();
            updateDataSet();
        }
        public void button3_Click(object sender, EventArgs e)
        {
            this.Close();

        }

 
        private void FilterGroupBox_Enter(object sender, EventArgs e)
        {

        }

        private void editbtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Request r = new Request();
            r.is_edit = true;
            r.request_id = (int) dataGridView1.SelectedRows[0].Cells[0].Value;
            r.ShowDialog();
            this.Show();
            updateDataSet();
        }

        private void closeRequestbtn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0) {
                if(listOfReqViewDataset.listOfRequests.FindById((int)dataGridView1.SelectedRows[0].Cells[0].Value).status != 1)
                {
                    MessageBox.Show("اظهارنامه فوق باز نیست");
                    return;
                }
                if(RequestController.getInstance().closeRequest( (int)dataGridView1.SelectedRows[0].Cells[0].Value))
                {
                    MessageBox.Show("با موفقیت بسته شد");
                    updateDataSet();
                }
                else
                {
                    MessageBox.Show("بعضی مجوزها کامل نیستند");
                }
            }
        }
    }
}
