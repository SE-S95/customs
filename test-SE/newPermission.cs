﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_SE
{
    public partial class newPermission : Form
    {
        public newPermission()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int duration = decimal.ToInt32(numericUpDown1.Value + numericUpDown2.Value * 30 + numericUpDown3.Value * 365);
            int result = permissionTableAdapter1.Insert(textBox1.Text, duration, richTextBox1.Text, int.Parse(textBox4.Text != ""? textBox4.Text : "0"), int.Parse(textBox2.Text != "" ? textBox2.Text : "0"), long.Parse(textBox3.Text != "" ? textBox3.Text : "0"), (int)comboBox1.SelectedValue);
            this.Close();
            
        }

        private void newPermission_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sadDataSet2.country' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.sadDataSet2.country);

        }
    }
}
