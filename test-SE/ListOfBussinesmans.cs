﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_SE
{
    public partial class ListOfBussinesmans : Form
    {
        public bool can_issue_permission = false;
        public bool can_choose = false;
        public ListOfBussinesmans()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count ==0 )
            {
                MessageBox.Show("Choose businessman!");
                return;
            }
            ListOfPermissions l = new ListOfPermissions();
            l.can_choose = true;
            if(l.ShowDialog() == DialogResult.OK)
                businessman_permsTableAdapter1.Insert(l.choosen_per.id, (int)dataGridView1.SelectedRows[0].Cells[0].Value, 0);
        }

        private void ListOfBussinesmans_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sadDataSet3.bussinesman' table. You can move, or remove it, as needed.
            this.bussinesmanTableAdapter.Fill(this.sadDataSet3.bussinesman);
            issuePerbtn.Visible = can_issue_permission;
            choosebtn.Visible = can_choose;
        }

        private void newbtn_Click(object sender, EventArgs e)
        {
            //newBussinesman newb = new newBussinesman();

            dataGridView1.EndEdit();
            bussinesmanTableAdapter.Update(sadDataSet3);
        }

        
    }
}
