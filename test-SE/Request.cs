﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using test_SE.Controller;
namespace test_SE
{
    public partial class Request : Form
    {
        public bool is_edit = false;
        public int request_id = 0;
        public Request()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (is_edit == false)
            {
                requestTableAdapter1.Insert((int)comboBox1.SelectedValue, dateTimePicker1.Value, groundRadio.Checked ? groundRadio.Text : airRadio.Checked ? airRadio.Text : seaRadio.Text, (int)countryCombo.SelectedValue, int.Parse(textBox2.Text), 1);
                int last = RequestController.getInstance().getLastReqId();
                foreach (DataRow rw in requestDataSet.request_item.Rows)
                {
                    request_itemTableAdapter.Insert(last, int.Parse(rw["good_id"].ToString()), int.Parse(rw["weight"].ToString()), rw["company"].ToString(), int.Parse(rw["quantity"].ToString()), long.Parse(rw["price"].ToString()));
                }
                RequestController.getInstance().insertRules(last);
            }
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            CheckForm checking = new CheckForm();
            checking.request_id = this.request_id;
            checking.ShowDialog();
            this.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RequestController ReqCont1 = RequestController.getInstance();

            ListOfBussinesmans listofbs = new ListOfBussinesmans();
            listofbs.can_choose = true;
            listofbs.ShowDialog();
        }

        private void Request_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sadDataSet.Good' table. You can move, or remove it, as needed.
            this.goodTableAdapter.FillByNotCat(this.sadDataSet.Good);
            // TODO: This line of code loads data into the 'sadDataSet2.country' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.sadDataSet2.country);
            // TODO: This line of code loads data into the 'sadDataSet3.bussinesman' table. You can move, or remove it, as needed.
            this.bussinesmanTableAdapter.Fill(this.sadDataSet3.bussinesman);

            if (this.is_edit == false)
                button3.Visible = false;
            else
            {
                button3.Visible = true;
                requestTableAdapter1.FillBy(requestDataSet.request, this.request_id);
                request_itemTableAdapter.FillBy(requestDataSet.request_item, this.request_id);
                comboBox1.Enabled = false;

                comboBox1.SelectedValue = sadDataSet3.bussinesman.FindById((int)requestDataSet.request.Rows[0]["businessman"]).Id;
                countryCombo.SelectedValue = sadDataSet2.country.FindById((int)requestDataSet.request.Rows[0]["country"]).Id;
                switch (requestDataSet.request.Rows[0]["ways"].ToString())
                {
                    case "هوایی":
                        airRadio.Checked = true;
                        break;
                    case "زمینی":
                        groundRadio.Checked = true;
                        break;
                    case "دریایی":
                        seaRadio.Checked = true;
                        break;
                }
                dateTimePicker1.Value = (DateTime)requestDataSet.request.Rows[0]["reqDate"];
                textBox2.Text = requestDataSet.request.Rows[0]["totalcost"].ToString();
                dateTimePicker1.Enabled = false;
                countryCombo.Enabled = false;
                airRadio.Enabled = false;
                groundRadio.Enabled = false;
                seaRadio.Enabled = false;
                dataGridView1.ReadOnly = true;
            }
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name;
            name = RequestController.getInstance().getBusinessmanName((int) comboBox1.SelectedValue);
            first_t.Text = name;
        }

        private void ButtonPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void fillByNotCatToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.goodTableAdapter.FillByNotCat(this.sadDataSet.Good);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int result = 0;
            foreach(DataRow dr in requestDataSet.request_item.Rows)
            {
                result += int.Parse(dr["price"].ToString()) * int.Parse(dr["quantity"].ToString());
                
            }
            textBox2.Text = result.ToString();
        }

        
    }
}
