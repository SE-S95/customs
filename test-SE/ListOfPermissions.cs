﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_SE
{
    public partial class ListOfPermissions : Form
    {
        public struct per
        {
            public int id;
            public string title;
        }
        public per choosen_per;
        public bool can_add = false;
        public bool can_choose = false;
        
        public ListOfPermissions()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListOfPermissions_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'permissionDataSet.Permission' table. You can move, or remove it, as needed.
            this.permissionTableAdapter.ClearBeforeFill = true;
            this.permissionTableAdapter.Fill(this.permissionDataSet.Permission);
            choosebtn.Visible = can_choose;
            newbtn.Visible = can_add;
        }

        private void choosebtn_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count > 0)
            {
                choosen_per = new per();
                choosen_per.id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                choosen_per.title = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
                this.DialogResult = DialogResult.OK;
                this.Close();
                return;
            }
            MessageBox.Show("Choose per!");
            
        }

        private void newbtn_Click(object sender, EventArgs e)
        {
            newPermission np = new newPermission();
            np.ShowDialog();
            this.permissionTableAdapter.Fill(this.permissionDataSet.Permission);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
