﻿namespace test_SE
{
    partial class ListOfBussinesmans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ButtonPanel = new System.Windows.Forms.Panel();
            this.newbtn = new System.Windows.Forms.Button();
            this.choosebtn = new System.Windows.Forms.Button();
            this.exitbtn = new System.Windows.Forms.Button();
            this.issuePerbtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ssnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bussinesmanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sadDataSet3 = new test_SE.sadDataSet3();
            this.bussinesmanTableAdapter = new test_SE.sadDataSet3TableAdapters.bussinesmanTableAdapter();
            this.businessman_permsTableAdapter1 = new test_SE.buspermsDataSetTableAdapters.businessman_permsTableAdapter();
            this.ButtonPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bussinesmanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet3)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonPanel
            // 
            this.ButtonPanel.Controls.Add(this.newbtn);
            this.ButtonPanel.Controls.Add(this.choosebtn);
            this.ButtonPanel.Controls.Add(this.exitbtn);
            this.ButtonPanel.Controls.Add(this.issuePerbtn);
            this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonPanel.Location = new System.Drawing.Point(0, 363);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.Size = new System.Drawing.Size(643, 47);
            this.ButtonPanel.TabIndex = 12;
            // 
            // newbtn
            // 
            this.newbtn.Location = new System.Drawing.Point(550, 12);
            this.newbtn.Name = "newbtn";
            this.newbtn.Size = new System.Drawing.Size(85, 23);
            this.newbtn.TabIndex = 2;
            this.newbtn.Text = "ذخیره";
            this.newbtn.UseVisualStyleBackColor = true;
            this.newbtn.Click += new System.EventHandler(this.newbtn_Click);
            // 
            // choosebtn
            // 
            this.choosebtn.Location = new System.Drawing.Point(469, 12);
            this.choosebtn.Name = "choosebtn";
            this.choosebtn.Size = new System.Drawing.Size(75, 23);
            this.choosebtn.TabIndex = 1;
            this.choosebtn.Text = "انتخاب";
            this.choosebtn.UseVisualStyleBackColor = true;
            // 
            // exitbtn
            // 
            this.exitbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.exitbtn.Location = new System.Drawing.Point(27, 12);
            this.exitbtn.Name = "exitbtn";
            this.exitbtn.Size = new System.Drawing.Size(96, 23);
            this.exitbtn.TabIndex = 0;
            this.exitbtn.Text = "خروج";
            this.exitbtn.UseVisualStyleBackColor = true;
            this.exitbtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // issuePerbtn
            // 
            this.issuePerbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.issuePerbtn.Location = new System.Drawing.Point(129, 12);
            this.issuePerbtn.Name = "issuePerbtn";
            this.issuePerbtn.Size = new System.Drawing.Size(96, 23);
            this.issuePerbtn.TabIndex = 0;
            this.issuePerbtn.Text = "صدور مجوز";
            this.issuePerbtn.UseVisualStyleBackColor = true;
            this.issuePerbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.fnameDataGridViewTextBoxColumn,
            this.lnameDataGridViewTextBoxColumn,
            this.ssnDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.bussinesmanBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 410);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "شماره";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fnameDataGridViewTextBoxColumn
            // 
            this.fnameDataGridViewTextBoxColumn.DataPropertyName = "fname";
            this.fnameDataGridViewTextBoxColumn.HeaderText = "نام";
            this.fnameDataGridViewTextBoxColumn.Name = "fnameDataGridViewTextBoxColumn";
            // 
            // lnameDataGridViewTextBoxColumn
            // 
            this.lnameDataGridViewTextBoxColumn.DataPropertyName = "lname";
            this.lnameDataGridViewTextBoxColumn.HeaderText = "نام خانوادگی";
            this.lnameDataGridViewTextBoxColumn.Name = "lnameDataGridViewTextBoxColumn";
            // 
            // ssnDataGridViewTextBoxColumn
            // 
            this.ssnDataGridViewTextBoxColumn.DataPropertyName = "ssn";
            this.ssnDataGridViewTextBoxColumn.HeaderText = "کدملی";
            this.ssnDataGridViewTextBoxColumn.Name = "ssnDataGridViewTextBoxColumn";
            // 
            // bussinesmanBindingSource
            // 
            this.bussinesmanBindingSource.DataMember = "bussinesman";
            this.bussinesmanBindingSource.DataSource = this.sadDataSet3;
            // 
            // sadDataSet3
            // 
            this.sadDataSet3.DataSetName = "sadDataSet3";
            this.sadDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bussinesmanTableAdapter
            // 
            this.bussinesmanTableAdapter.ClearBeforeFill = true;
            // 
            // businessman_permsTableAdapter1
            // 
            this.businessman_permsTableAdapter1.ClearBeforeFill = true;
            // 
            // ListOfBussinesmans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 410);
            this.Controls.Add(this.ButtonPanel);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ListOfBussinesmans";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "فهرست تجار";
            this.Load += new System.EventHandler(this.ListOfBussinesmans_Load);
            this.ButtonPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bussinesmanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sadDataSet3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel ButtonPanel;
        private System.Windows.Forms.Button exitbtn;
        private System.Windows.Forms.Button issuePerbtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button choosebtn;
        private System.Windows.Forms.Button newbtn;
        private sadDataSet3 sadDataSet3;
        private System.Windows.Forms.BindingSource bussinesmanBindingSource;
        private sadDataSet3TableAdapters.bussinesmanTableAdapter bussinesmanTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ssnDataGridViewTextBoxColumn;
        private buspermsDataSetTableAdapters.businessman_permsTableAdapter businessman_permsTableAdapter1;
    }
}