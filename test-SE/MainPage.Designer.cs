﻿namespace test_SE
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.userName = new System.Windows.Forms.TextBox();
            this.userID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.اظهارنامهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.جدیدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.لیستاظهارنامههاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.افزودنتاجرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مدیریتسیستمToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تعریفمجوزToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مدیریتقوانینToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.اضافهکردنکشورToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اضافهکردنکالاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.لیستتجادToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.تعریفکاربرجدیدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.خروجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.userName);
            this.groupBox1.Controls.Add(this.userID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(644, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(213, 95);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مشخصات کاربر";
            // 
            // userName
            // 
            this.userName.BackColor = System.Drawing.SystemColors.Info;
            this.userName.Enabled = false;
            this.userName.Location = new System.Drawing.Point(22, 62);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(98, 21);
            this.userName.TabIndex = 1;
            this.userName.Text = "محمدی";
            // 
            // userID
            // 
            this.userID.BackColor = System.Drawing.SystemColors.Info;
            this.userID.Enabled = false;
            this.userID.Location = new System.Drawing.Point(22, 37);
            this.userID.Name = "userID";
            this.userID.Size = new System.Drawing.Size(98, 21);
            this.userID.TabIndex = 1;
            this.userID.Text = "محمد";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "نام‌کاربری";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(151, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "کد‌کاربری";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Location = new System.Drawing.Point(12, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(371, 160);
            this.label4.TabIndex = 1;
            this.label4.Text = "۱۳۹۵";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.اظهارنامهToolStripMenuItem,
            this.مدیریتسیستمToolStripMenuItem,
            this.خروجToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(869, 27);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // اظهارنامهToolStripMenuItem
            // 
            this.اظهارنامهToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.جدیدToolStripMenuItem,
            this.لیستاظهارنامههاToolStripMenuItem,
            this.افزودنتاجرToolStripMenuItem});
            this.اظهارنامهToolStripMenuItem.Name = "اظهارنامهToolStripMenuItem";
            this.اظهارنامهToolStripMenuItem.Size = new System.Drawing.Size(82, 23);
            this.اظهارنامهToolStripMenuItem.Text = "اظهارنامه";
            // 
            // جدیدToolStripMenuItem
            // 
            this.جدیدToolStripMenuItem.Name = "جدیدToolStripMenuItem";
            this.جدیدToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.جدیدToolStripMenuItem.Text = "جدید";
            this.جدیدToolStripMenuItem.Click += new System.EventHandler(this.جدیدToolStripMenuItem_Click);
            // 
            // لیستاظهارنامههاToolStripMenuItem
            // 
            this.لیستاظهارنامههاToolStripMenuItem.Name = "لیستاظهارنامههاToolStripMenuItem";
            this.لیستاظهارنامههاToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.لیستاظهارنامههاToolStripMenuItem.Text = "لیست اظهارنامه‌ها";
            this.لیستاظهارنامههاToolStripMenuItem.Click += new System.EventHandler(this.لیستاظهارنامههاToolStripMenuItem_Click);
            // 
            // افزودنتاجرToolStripMenuItem
            // 
            this.افزودنتاجرToolStripMenuItem.Name = "افزودنتاجرToolStripMenuItem";
            this.افزودنتاجرToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.افزودنتاجرToolStripMenuItem.Text = "لیست تجار";
            this.افزودنتاجرToolStripMenuItem.Click += new System.EventHandler(this.افزودنتاجرToolStripMenuItem_Click);
            // 
            // مدیریتسیستمToolStripMenuItem
            // 
            this.مدیریتسیستمToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.تعریفمجوزToolStripMenuItem,
            this.مدیریتقوانینToolStripMenuItem,
            this.toolStripMenuItem2,
            this.اضافهکردنکشورToolStripMenuItem,
            this.اضافهکردنکالاToolStripMenuItem,
            this.لیستتجادToolStripMenuItem,
            this.toolStripMenuItem1,
            this.تعریفکاربرجدیدToolStripMenuItem});
            this.مدیریتسیستمToolStripMenuItem.Name = "مدیریتسیستمToolStripMenuItem";
            this.مدیریتسیستمToolStripMenuItem.Size = new System.Drawing.Size(129, 23);
            this.مدیریتسیستمToolStripMenuItem.Text = "مدیریت سیستم";
            // 
            // تعریفمجوزToolStripMenuItem
            // 
            this.تعریفمجوزToolStripMenuItem.Name = "تعریفمجوزToolStripMenuItem";
            this.تعریفمجوزToolStripMenuItem.Size = new System.Drawing.Size(189, 24);
            this.تعریفمجوزToolStripMenuItem.Text = "مدیریت مجوزها";
            this.تعریفمجوزToolStripMenuItem.Click += new System.EventHandler(this.تعریفمجوزToolStripMenuItem_Click);
            // 
            // مدیریتقوانینToolStripMenuItem
            // 
            this.مدیریتقوانینToolStripMenuItem.Name = "مدیریتقوانینToolStripMenuItem";
            this.مدیریتقوانینToolStripMenuItem.Size = new System.Drawing.Size(189, 24);
            this.مدیریتقوانینToolStripMenuItem.Text = "مدیریت قوانین";
            this.مدیریتقوانینToolStripMenuItem.Click += new System.EventHandler(this.مدیریتقوانینToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(186, 6);
            // 
            // اضافهکردنکشورToolStripMenuItem
            // 
            this.اضافهکردنکشورToolStripMenuItem.Name = "اضافهکردنکشورToolStripMenuItem";
            this.اضافهکردنکشورToolStripMenuItem.Size = new System.Drawing.Size(189, 24);
            this.اضافهکردنکشورToolStripMenuItem.Text = "لیست کشورها";
            this.اضافهکردنکشورToolStripMenuItem.Click += new System.EventHandler(this.اضافهکردنکشورToolStripMenuItem_Click);
            // 
            // اضافهکردنکالاToolStripMenuItem
            // 
            this.اضافهکردنکالاToolStripMenuItem.Name = "اضافهکردنکالاToolStripMenuItem";
            this.اضافهکردنکالاToolStripMenuItem.Size = new System.Drawing.Size(189, 24);
            this.اضافهکردنکالاToolStripMenuItem.Text = "لیست کالاها";
            this.اضافهکردنکالاToolStripMenuItem.Click += new System.EventHandler(this.اضافهکردنکالاToolStripMenuItem_Click);
            // 
            // لیستتجادToolStripMenuItem
            // 
            this.لیستتجادToolStripMenuItem.Name = "لیستتجادToolStripMenuItem";
            this.لیستتجادToolStripMenuItem.Size = new System.Drawing.Size(189, 24);
            this.لیستتجادToolStripMenuItem.Text = "لیست تجار";
            this.لیستتجادToolStripMenuItem.Click += new System.EventHandler(this.لیستتجادToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(186, 6);
            // 
            // تعریفکاربرجدیدToolStripMenuItem
            // 
            this.تعریفکاربرجدیدToolStripMenuItem.Name = "تعریفکاربرجدیدToolStripMenuItem";
            this.تعریفکاربرجدیدToolStripMenuItem.Size = new System.Drawing.Size(189, 24);
            this.تعریفکاربرجدیدToolStripMenuItem.Text = "تعریف کاربر جدید";
            this.تعریفکاربرجدیدToolStripMenuItem.Click += new System.EventHandler(this.تعریفکاربرجدیدToolStripMenuItem_Click);
            // 
            // خروجToolStripMenuItem
            // 
            this.خروجToolStripMenuItem.Name = "خروجToolStripMenuItem";
            this.خروجToolStripMenuItem.Size = new System.Drawing.Size(56, 23);
            this.خروجToolStripMenuItem.Text = "خروج";
            this.خروجToolStripMenuItem.Click += new System.EventHandler(this.خروجToolStripMenuItem_Click);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 331);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainPage";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "صفحه اصلی";
            this.Load += new System.EventHandler(this.MainPage_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.TextBox userID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem اظهارنامهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem جدیدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem لیستاظهارنامههاToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem خروجToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مدیریتسیستمToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تعریفمجوزToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مدیریتقوانینToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem تعریفکاربرجدیدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem افزودنتاجرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem لیستتجادToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem اضافهکردنکالاToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اضافهکردنکشورToolStripMenuItem;
    }
}