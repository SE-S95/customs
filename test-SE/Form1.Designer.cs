﻿namespace test_SE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.first_l = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.reqTab = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.totalPrice = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.isOne = new System.Windows.Forms.CheckBox();
            this.verified = new System.Windows.Forms.CheckBox();
            this.PermissionsBtn = new System.Windows.Forms.Button();
            this.transportationType = new System.Windows.Forms.GroupBox();
            this.groundRadio = new System.Windows.Forms.RadioButton();
            this.airRadio = new System.Windows.Forms.RadioButton();
            this.seaRadio = new System.Windows.Forms.RadioButton();
            this.country_t = new System.Windows.Forms.TextBox();
            this.country_l = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dollar = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PriceLabel = new System.Windows.Forms.Label();
            this.countLabel = new System.Windows.Forms.Label();
            this.weightLabel = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.company_l = new System.Windows.Forms.Label();
            this.good_l = new System.Windows.Forms.Label();
            this.company = new System.Windows.Forms.TextBox();
            this.good_name = new System.Windows.Forms.TextBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.data_l = new System.Windows.Forms.Label();
            this.ssn_t = new System.Windows.Forms.TextBox();
            this.ssn_l = new System.Windows.Forms.Label();
            this.last_l = new System.Windows.Forms.Label();
            this.last_t = new System.Windows.Forms.TextBox();
            this.first_t = new System.Windows.Forms.TextBox();
            this.validTab = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.reqTab.SuspendLayout();
            this.transportationType.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // first_l
            // 
            this.first_l.AutoSize = true;
            this.first_l.Location = new System.Drawing.Point(759, 31);
            this.first_l.Name = "first_l";
            this.first_l.Size = new System.Drawing.Size(20, 13);
            this.first_l.TabIndex = 2;
            this.first_l.Text = "نام";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.reqTab);
            this.tabControl1.Controls.Add(this.validTab);
            this.tabControl1.Location = new System.Drawing.Point(-2, -2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(837, 495);
            this.tabControl1.TabIndex = 17;
            // 
            // reqTab
            // 
            this.reqTab.Controls.Add(this.label2);
            this.reqTab.Controls.Add(this.first_l);
            this.reqTab.Controls.Add(this.totalPrice);
            this.reqTab.Controls.Add(this.label1);
            this.reqTab.Controls.Add(this.isOne);
            this.reqTab.Controls.Add(this.verified);
            this.reqTab.Controls.Add(this.PermissionsBtn);
            this.reqTab.Controls.Add(this.transportationType);
            this.reqTab.Controls.Add(this.country_t);
            this.reqTab.Controls.Add(this.country_l);
            this.reqTab.Controls.Add(this.panel1);
            this.reqTab.Controls.Add(this.monthCalendar1);
            this.reqTab.Controls.Add(this.data_l);
            this.reqTab.Controls.Add(this.ssn_t);
            this.reqTab.Controls.Add(this.ssn_l);
            this.reqTab.Controls.Add(this.last_l);
            this.reqTab.Controls.Add(this.last_t);
            this.reqTab.Controls.Add(this.first_t);
            this.reqTab.Location = new System.Drawing.Point(4, 22);
            this.reqTab.Name = "reqTab";
            this.reqTab.Padding = new System.Windows.Forms.Padding(3);
            this.reqTab.Size = new System.Drawing.Size(829, 469);
            this.reqTab.TabIndex = 0;
            this.reqTab.Text = "اظهارنامه";
            this.reqTab.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 318);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "دلار";
            // 
            // totalPrice
            // 
            this.totalPrice.AutoSize = true;
            this.totalPrice.Location = new System.Drawing.Point(144, 318);
            this.totalPrice.Name = "totalPrice";
            this.totalPrice.Size = new System.Drawing.Size(13, 13);
            this.totalPrice.TabIndex = 33;
            this.totalPrice.Text = "۰";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(190, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "ارزش کل کالا:";
            // 
            // isOne
            // 
            this.isOne.AutoSize = true;
            this.isOne.Enabled = false;
            this.isOne.Location = new System.Drawing.Point(338, 314);
            this.isOne.Name = "isOne";
            this.isOne.Size = new System.Drawing.Size(97, 17);
            this.isOne.TabIndex = 31;
            this.isOne.Text = "بیشتر از یک کالا";
            this.isOne.UseVisualStyleBackColor = true;
            // 
            // verified
            // 
            this.verified.AutoSize = true;
            this.verified.Enabled = false;
            this.verified.Location = new System.Drawing.Point(144, 417);
            this.verified.Name = "verified";
            this.verified.Size = new System.Drawing.Size(47, 17);
            this.verified.TabIndex = 30;
            this.verified.Text = "تایید";
            this.verified.UseVisualStyleBackColor = true;
            // 
            // PermissionsBtn
            // 
            this.PermissionsBtn.Location = new System.Drawing.Point(36, 411);
            this.PermissionsBtn.Name = "PermissionsBtn";
            this.PermissionsBtn.Size = new System.Drawing.Size(102, 26);
            this.PermissionsBtn.TabIndex = 28;
            this.PermissionsBtn.Text = "مجوز‌ها";
            this.PermissionsBtn.UseVisualStyleBackColor = true;
            // 
            // transportationType
            // 
            this.transportationType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transportationType.Controls.Add(this.groundRadio);
            this.transportationType.Controls.Add(this.airRadio);
            this.transportationType.Controls.Add(this.seaRadio);
            this.transportationType.Location = new System.Drawing.Point(497, 314);
            this.transportationType.Name = "transportationType";
            this.transportationType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.transportationType.Size = new System.Drawing.Size(281, 45);
            this.transportationType.TabIndex = 27;
            this.transportationType.TabStop = false;
            this.transportationType.Text = "نحوه ورود به کشور";
            // 
            // groundRadio
            // 
            this.groundRadio.AutoSize = true;
            this.groundRadio.Location = new System.Drawing.Point(190, 20);
            this.groundRadio.Name = "groundRadio";
            this.groundRadio.Size = new System.Drawing.Size(53, 17);
            this.groundRadio.TabIndex = 2;
            this.groundRadio.TabStop = true;
            this.groundRadio.Text = "زمینی";
            this.groundRadio.UseVisualStyleBackColor = true;
            // 
            // airRadio
            // 
            this.airRadio.AutoSize = true;
            this.airRadio.Location = new System.Drawing.Point(99, 21);
            this.airRadio.Name = "airRadio";
            this.airRadio.Size = new System.Drawing.Size(55, 17);
            this.airRadio.TabIndex = 1;
            this.airRadio.TabStop = true;
            this.airRadio.Text = "هوایی";
            this.airRadio.UseVisualStyleBackColor = true;
            // 
            // seaRadio
            // 
            this.seaRadio.AutoSize = true;
            this.seaRadio.Location = new System.Drawing.Point(7, 20);
            this.seaRadio.Name = "seaRadio";
            this.seaRadio.Size = new System.Drawing.Size(57, 17);
            this.seaRadio.TabIndex = 0;
            this.seaRadio.TabStop = true;
            this.seaRadio.Text = "دریایی";
            this.seaRadio.UseVisualStyleBackColor = true;
            // 
            // country_t
            // 
            this.country_t.Location = new System.Drawing.Point(494, 275);
            this.country_t.Name = "country_t";
            this.country_t.Size = new System.Drawing.Size(196, 20);
            this.country_t.TabIndex = 26;
            // 
            // country_l
            // 
            this.country_l.AutoSize = true;
            this.country_l.Location = new System.Drawing.Point(727, 278);
            this.country_l.Name = "country_l";
            this.country_l.Size = new System.Drawing.Size(54, 13);
            this.country_l.TabIndex = 25;
            this.country_l.Text = "کشور مبدا";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dollar);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.PriceLabel);
            this.panel1.Controls.Add(this.countLabel);
            this.panel1.Controls.Add(this.weightLabel);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.company_l);
            this.panel1.Controls.Add(this.good_l);
            this.panel1.Controls.Add(this.company);
            this.panel1.Controls.Add(this.good_name);
            this.panel1.Location = new System.Drawing.Point(23, 103);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(374, 183);
            this.panel1.TabIndex = 24;
            // 
            // dollar
            // 
            this.dollar.AutoSize = true;
            this.dollar.Location = new System.Drawing.Point(99, 138);
            this.dollar.Name = "dollar";
            this.dollar.Size = new System.Drawing.Size(23, 13);
            this.dollar.TabIndex = 11;
            this.dollar.Text = "دلار";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(78, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "کیلوگرم";
            // 
            // PriceLabel
            // 
            this.PriceLabel.AutoSize = true;
            this.PriceLabel.Location = new System.Drawing.Point(254, 141);
            this.PriceLabel.Name = "PriceLabel";
            this.PriceLabel.Size = new System.Drawing.Size(74, 13);
            this.PriceLabel.TabIndex = 9;
            this.PriceLabel.Text = "قیمت واحد کالا";
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Location = new System.Drawing.Point(254, 116);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(30, 13);
            this.countLabel.TabIndex = 8;
            this.countLabel.Text = "تعداد";
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Location = new System.Drawing.Point(254, 90);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(53, 13);
            this.weightLabel.TabIndex = 7;
            this.weightLabel.Text = "وزن خالص";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(137, 135);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 6;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(137, 109);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(137, 83);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            // 
            // company_l
            // 
            this.company_l.AutoSize = true;
            this.company_l.Location = new System.Drawing.Point(254, 58);
            this.company_l.Name = "company_l";
            this.company_l.Size = new System.Drawing.Size(72, 13);
            this.company_l.TabIndex = 3;
            this.company_l.Text = "شرکت سازنده";
            // 
            // good_l
            // 
            this.good_l.AutoSize = true;
            this.good_l.Location = new System.Drawing.Point(254, 26);
            this.good_l.Name = "good_l";
            this.good_l.Size = new System.Drawing.Size(40, 13);
            this.good_l.TabIndex = 2;
            this.good_l.Text = "نام کالا";
            // 
            // company
            // 
            this.company.Location = new System.Drawing.Point(34, 51);
            this.company.Name = "company";
            this.company.Size = new System.Drawing.Size(203, 20);
            this.company.TabIndex = 1;
            // 
            // good_name
            // 
            this.good_name.Location = new System.Drawing.Point(34, 23);
            this.good_name.Name = "good_name";
            this.good_name.Size = new System.Drawing.Size(203, 20);
            this.good_name.TabIndex = 0;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(490, 99);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 23;
            // 
            // data_l
            // 
            this.data_l.AutoSize = true;
            this.data_l.Location = new System.Drawing.Point(729, 99);
            this.data_l.Name = "data_l";
            this.data_l.Size = new System.Drawing.Size(63, 13);
            this.data_l.TabIndex = 22;
            this.data_l.Text = "تاریخ احضار";
            // 
            // ssn_t
            // 
            this.ssn_t.Location = new System.Drawing.Point(494, 60);
            this.ssn_t.Name = "ssn_t";
            this.ssn_t.Size = new System.Drawing.Size(197, 20);
            this.ssn_t.TabIndex = 21;
            // 
            // ssn_l
            // 
            this.ssn_l.AutoSize = true;
            this.ssn_l.Location = new System.Drawing.Point(727, 61);
            this.ssn_l.Name = "ssn_l";
            this.ssn_l.Size = new System.Drawing.Size(59, 13);
            this.ssn_l.TabIndex = 20;
            this.ssn_l.Text = "شناسه ملی";
            // 
            // last_l
            // 
            this.last_l.AutoSize = true;
            this.last_l.Location = new System.Drawing.Point(382, 32);
            this.last_l.Name = "last_l";
            this.last_l.Size = new System.Drawing.Size(69, 13);
            this.last_l.TabIndex = 19;
            this.last_l.Text = "نام خانوادگی";
            // 
            // last_t
            // 
            this.last_t.Location = new System.Drawing.Point(160, 28);
            this.last_t.Name = "last_t";
            this.last_t.Size = new System.Drawing.Size(196, 20);
            this.last_t.TabIndex = 18;
            // 
            // first_t
            // 
            this.first_t.Location = new System.Drawing.Point(494, 28);
            this.first_t.Name = "first_t";
            this.first_t.Size = new System.Drawing.Size(197, 20);
            this.first_t.TabIndex = 17;
            // 
            // validTab
            // 
            this.validTab.Location = new System.Drawing.Point(4, 22);
            this.validTab.Name = "validTab";
            this.validTab.Padding = new System.Windows.Forms.Padding(3);
            this.validTab.Size = new System.Drawing.Size(829, 469);
            this.validTab.TabIndex = 1;
            this.validTab.Text = "مجوز‌ها";
            this.validTab.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 479);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.reqTab.ResumeLayout(false);
            this.reqTab.PerformLayout();
            this.transportationType.ResumeLayout(false);
            this.transportationType.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label first_l;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage reqTab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label totalPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox isOne;
        private System.Windows.Forms.CheckBox verified;
        private System.Windows.Forms.Button PermissionsBtn;
        private System.Windows.Forms.GroupBox transportationType;
        private System.Windows.Forms.RadioButton groundRadio;
        private System.Windows.Forms.RadioButton airRadio;
        private System.Windows.Forms.RadioButton seaRadio;
        private System.Windows.Forms.TextBox country_t;
        private System.Windows.Forms.Label country_l;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label dollar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label PriceLabel;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label company_l;
        private System.Windows.Forms.Label good_l;
        private System.Windows.Forms.TextBox company;
        private System.Windows.Forms.TextBox good_name;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label data_l;
        private System.Windows.Forms.TextBox ssn_t;
        private System.Windows.Forms.Label ssn_l;
        private System.Windows.Forms.Label last_l;
        private System.Windows.Forms.TextBox last_t;
        private System.Windows.Forms.TextBox first_t;
        private System.Windows.Forms.TabPage validTab;
    }
}

