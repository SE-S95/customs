﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_SE
{
    public partial class ListOfCountries : Form
    {
        public ListOfCountries()
        {
            InitializeComponent();
        }

        private void ListOfCountries_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sadDataSet2.country' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.sadDataSet2.country);

        }

        private void extBtn_Click(object sender, EventArgs e)
        {
            countryBindingSource.EndEdit();
            countryTableAdapter.Update(sadDataSet2);
            this.Close();
        }
    }
}
