﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace test_SE.Model
{
    class Good
    {
        public int id { get; set; }
        public int parentid { get; set; }
        public string title { get; set; }
        public Good(int id, int parent, string title)
        {
            this.id = id;
            this.parentid = parent;
            this.title = title;
        }

        public static List<Good> all()
        {
            List<Good> result = null;
            DBConnection db = DBConnection.getInstance();
            string query = "SELECT * FROM GOOD";
            db.cmm.CommandText = query;
            SqlDataReader dr = db.cmm.ExecuteReader();
            if (dr.HasRows)
            {
                result = new List<Good>();
                while (dr.Read())
                {
                    if(dr[1] == DBNull.Value)
                        result.Add(new Good((int)dr[0], 0 , dr[2].ToString()));
                    else
                    {
                        result.Add(new Good((int)dr[0], (int) dr[1], dr[2].ToString()));
                    }
                }
            }
            dr.Close();
            return result;
        }

        public Good getParent()
        {
            Good result = null;
            DBConnection db = DBConnection.getInstance();
            string query = "SELECT * FROM GOOD WHERE ID =" + this.parentid.ToString();
            db.cmm.CommandText = query;
            SqlDataReader dr = db.cmm.ExecuteReader();
            if (dr.HasRows)
            {
                result = new Good((int)dr[0], 0, dr[2].ToString());
            }
            return result;
        }

        public bool save()
        {
            DBConnection db = DBConnection.getInstance();
            string query;
            if (id == 0)
            {
                query = "INSERT INTO GOOD VALUES(" + this.parentid + ", '" + this.title + "')";
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
