﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace test_SE.Model
{
    class Country
    {
        public int id { get; set; }
        public string code {get; set;}
        public string title { get; set; }

        public Country(int id,string code, string title)
        {
            this.id = id;
            this.code = code;
            this.title = title;
        }
        public static List<Country> all()
        {
            List<Country> result = null;
            DBConnection db = DBConnection.getInstance();
            string query = "SELECT * FROM COUNTRY";
            db.cmm.CommandText = query;
            SqlDataReader dr = db.cmm.ExecuteReader();
            if (dr.HasRows)
            {
                result = new List<Country>();
                while (dr.Read())
                {
                    result.Add(new Country((int)dr[0], dr[1].ToString(), dr[2].ToString()));
                }
            }
            dr.Close();
            return result;
        }
    }
}
