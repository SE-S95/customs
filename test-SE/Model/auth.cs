﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace test_SE.Model
{
    class auth
    {
        public int user_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public auth(int user_id, string username, string password)
        {
            this.user_id = user_id;
            this.username = username;
            this.password = password;
        }

        public Boolean checkAuth(string username, string password)
        {
            if(username == this.username && password == this.password)
            {
                return true;
            }
            return false;
        }

        public static auth getUser(string username, string password)
        {
            auth result = null;
            DBConnection db = DBConnection.getInstance();
            string query = "SELECT * FROM AUTH WHERE USERNAMe='" + username + "' AND PASSWORD='" + password + "'";
            db.cmm.CommandText = query;
            SqlDataReader dr = db.cmm.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                switch (dr["type"].ToString().Trim())
                {
                    case "Custom":
                        result = new CustomUser((int) dr[0], dr[1].ToString(), dr[2].ToString());
                        break;
                    case "Gov":
                        result = new GovUser((int)dr[0], dr[1].ToString(), dr[2].ToString());
                        break;
                    case "Legislator":
                        result = new LegislatorUser((int)dr[0], dr[1].ToString(), dr[2].ToString());
                        break;
                }
            }
            dr.Close();
            return result;
        }
    }
}
