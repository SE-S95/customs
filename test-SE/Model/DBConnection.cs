﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace test_SE.Model
{
    class DBConnection
    {
        private static DBConnection instance = null;
        private SqlConnection cnn = null;
        public SqlCommand cmm { get; set; }

        private DBConnection() {
            cnn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=sad;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            cnn.Open();
            cmm = new SqlCommand();
            cmm.Connection = cnn;
        }



        public static DBConnection getInstance()
        {
                if (instance == null)
                {
                    instance = new DBConnection();
                }
                return instance;
        }

    }
}
