﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using test_SE.Controller;
namespace test_SE
{
    public partial class NewRule : Form
    {
        public NewRule()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ruleTableAdapter1.Insert(
                checkBox3.Checked == false ? (int?) null : (int)comboBox2.SelectedValue,
                textBox4.Text == ""? (int?) null : int.Parse(textBox4.Text),
                textBox2.Text == ""? (int?) null : int.Parse(textBox2.Text),
                textBox3.Text == ""? (int?) null : int.Parse(textBox3.Text),
                checkBox4.Checked == false ? (int?) null : (int)comboBox1.SelectedValue,
                groundRadio.Checked ? groundRadio.Text : airRadio.Checked ? airRadio.Text : seaRadio.Text,
                checkBox1.Checked == false? (DateTime?)null : dateTimePicker1.Value,
                checkBox2.Checked == false? (DateTime?)null : dateTimePicker2.Value);
            int lastRuleID = RequestController.getInstance().getLastRuleId();
            foreach (DataRow dr in permissionDataSet.Permission.Rows) {
                rule_itemTableAdapter1.Insert(lastRuleID, (int) dr[0]);
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ListOfPermissions r = new ListOfPermissions();
            r.can_choose = true;
            DataRow row = this.permissionDataSet.Permission.NewRow();
            r.ShowDialog();
            row[0] = r.choosen_per.id;
            row[1] = r.choosen_per.title;
            if (row != null)
            {
                permissionDataSet.Permission.Rows.Add(row);
            }

        }

        private void NewRule_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sadDataSet2.country' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.sadDataSet2.country);
            // TODO: This line of code loads data into the 'sadDataSet.Good' table. You can move, or remove it, as needed.
            this.goodTableAdapter.FillBy(this.sadDataSet.Good);

        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.goodTableAdapter.FillBy(this.sadDataSet.Good);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void ButtonPanel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
