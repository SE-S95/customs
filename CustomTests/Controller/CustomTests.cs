﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using test_SE.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace custom.Tests
{
    [TestClass()]
    public class CustomTests
    {
        public SqlConnection cn;
        public SqlCommand sqlcmm;
        [TestInitialize]
        public void Setup()
        {
            cn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=sad;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            cn.Open();
            sqlcmm = new SqlCommand();
            sqlcmm.Connection = cn;
        }
        [TestMethod()]
        public void Vehicle()
        {
            sqlcmm.CommandText = "INSERT INTO REQUEST VALUES (3, '7/27/16', 'زمینی'" + ", 3, 1,1)";
            sqlcmm.ExecuteNonQuery();

            int lastId = RequestController.getInstance().getLastReqId();
            sqlcmm.CommandText = "INSERT INTO REQUEST_ITEM VALUES (" + lastId.ToString() + ", 3004, 10, 'Mehran co', 1,1)";
            sqlcmm.ExecuteNonQuery();

            RequestController.getInstance().insertRules(lastId);
            sqlcmm.CommandText = "SELECT count(*) FROM REQUIRED_PER WHERE REQ_ID=" + lastId.ToString();
            int result = (int)sqlcmm.ExecuteScalar();
            if (result == 2)
                return;
            else
                Assert.Fail();
        }

        [TestCleanup]
        public void TearDown()
        {
            cn.Close();
        }
    }
}